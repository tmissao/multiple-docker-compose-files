const express = require('express');
const cors = require('cors');
const cookieParser = require('cookie-parser');
const bodyParser = require('body-parser');
const handler = require('./src/handler').getHandler();

const app = express();

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(cors());
app.use((req, res, next) => { res.type('json'); next(); });

app.use('/', (req, res) => {
  res.send(JSON.stringify({ status: 'OK', message: handler.greetings() }));
});

module.exports = app;
