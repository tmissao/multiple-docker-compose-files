const constants = require('../config/config');

const _greetings = () => `This is a very secure secret: ${constants.secrets.mySecret} ## RunMode: ${constants.env.RUN_MODE}`;

const getHandler = () => ({
  greetings: _greetings
});

module.exports.getHandler = getHandler;
