# Multiples Docker-Compose Files

This tutorial aims to demonstrates how to solve a common problem using a Docker Solution for Development/Production environments, how to use different settings for different environments and preserve your sanity.

The current solution uses docker-compose resources and multiple docker-compose files.

## Development Environment

1. [Install Docker](https://docs.docker.com/install/linux/docker-ce/ubuntu/)
2. Create a Dockerfile for your application (In my case a build a node rest API)
``` yml
FROM node:9-alpine

# application placed into /opt/app
RUN mkdir -p /opt/app

WORKDIR /opt/app

# Install app dependencies
COPY package.json .
# For npm@5 or later, copy package-lock.json as well
# COPY package.json package-lock.json ./

RUN npm install --production && npm cache clean --force

COPY . .

EXPOSE 3000

CMD ["npm","start"]
```

3. Create a simple (really simple) docker-compose.yml file, with just services and service`s images
``` yml
version: '3.3'

services:

  api:
    image: api
```

4. Create a docker-compose.override.yml file, this file represents our development environment, so it must contain all your app configurations like: environment variables, secrets, ports, volumes, etc.

``` yml
version: '3.3'

services:

  api:
    build: .
    environment:
      RUN_MODE: 'DEVELOPMENT'
    secrets:
      - MY-SECURE-SECRET
    ports:
      - 3000:3000

secrets:
  MY-SECURE-SECRET:
    file: ./secrets/mysecret.txt
```

It is important to highlight that Docker will merge automatically our both files (docker-compose.yml and docker-compose.override.yml) when the command __*docker-compose up*__ be executed.

5. Run the command __*docker-compose up*__ and access on your browser the localhost url on port 3000 (__*127.0.0.1:3000*__) to get your API response.
``` json
{
   "status":"OK",
   "message":"This is a very secure secret: It works on compose ## RunMode: DEVELOPMENT"
}
```

After that your development environment is ready to go!

## Production Environment

For your production environment, Swarm will be used and is really important to keep in mind that Swarm is unable to build images, so the image must be ready to use.

1. Initialize Swarm
``` bash
docker swarm init
```

2. Build a secret. (This step it is necessary because your application is using the secret __*MY-SECURE-SECRET*__)
``` bash
echo "Production is Ready"| docker secret create MY-SECURE-SECRET -
```
3. Create a docker-compose.prod.yml file, this file represents our production environment, so it must contain all your app configurations like: production environment variables, secrets, ports, volumes, deploy configuration, etc.
``` yml
version: '3.3'

services:

  api:
    image: api
    deploy:
      replicas: 1
      update_config:
        parallelism: 1
        delay: 5s
      restart_policy:
        condition: on-failure
    environment:
      RUN_MODE: 'Production'
    secrets:
      - MY-SECURE-SECRET
    ports:
      - 80:3000


secrets:
  MY-SECURE-SECRET:
    external: true
```

4. Merge our docker-compose.yml and docker-compose.prod.yml ( the file order must be preserved, always from the base file to the specialized file ) creating a new file __*docker-stack.yml*__ . This process can be archived through the following command:
``` bash
docker-compose -f docker-compose.yml -f docker-compose.prod.yml config > docker-stack.yml
```

The result will be this __*docker-stack.yml*__
```yml
secrets:
  MY-SECURE-SECRET:
    external: true
services:
  api:
    deploy:
      replicas: 1
      restart_policy:
        condition: on-failure
      update_config:
        delay: 5s
        parallelism: 1
    environment:
      RUN_MODE: Production
    image: api
    ports:
    - published: 80
      target: 3000
    secrets:
    - source: MY-SECURE-SECRET
version: '3.3'

```

5. Build the App image since swarm will not be able to do.
``` bash
docker image build -t api .
```

6. Deploy the application using __*Docker Stack*__ and your docker-stack.yml file
``` bash
docker stack deploy -c docker-stack.yml api
```

7. Access on your browser the localhost url on port 80 (__*127.0.0.1:80*__) to get your API response.
``` json
{
   "status":"OK",
   "message":"This is a very secure secret: Production is Ready ## RunMode: Production"
}
```
That`s it folks, happy coding =) !!!
