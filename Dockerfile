FROM node:9-alpine

# application placed into /opt/app
RUN mkdir -p /opt/app

WORKDIR /opt/app

# Install app dependencies
COPY package.json .
# For npm@5 or later, copy package-lock.json as well
# COPY package.json package-lock.json ./

RUN npm install --production && npm cache clean --force

COPY . .

EXPOSE 3000

CMD ["npm","start"]
